#!/bin/bash

cd "$(dirname "$0")"

search_for_synpse_pod(){
    synapse_pod=$(oc get po -l 'app.kubernetes.io/component==synapse' -o name)
    [ -n "$synapse_pod" ]
}


try_to_determine_release_name(){
    RELEASE_NAME=$(helm ls | grep bundesmessenger | cut -d $'\t' -f 1)
    if [ -z "$RELEASE_NAME" ] || [ $(wc -l <<< "$RELEASE_NAME") -gt 1 ]; then
        echo "Failed to determine RELEASE_NAME"
		exit 1
    fi
}


main(){
    [ -n "$RELEASE_NAME" ] || try_to_determine_release_name

    if oc get cm -o name | grep bundesmessenger$ | xargs oc get -o yaml | grep -q oidc_providers:; then
        local use_mas=false
    else
        local use_mas=true
    fi

    local synapse_pod
    if ! search_for_synpse_pod; then
        echo "Waiting for the synapse pod ..."
        while [ -z "$synapse_pod" ]; do
            sleep 1
            echo -n "."
            search_for_synpse_pod
        done
    fi
    echo "Wait until $synapse_pod gets ready"
    oc wait --for=condition=Ready --timeout=1h "$synapse_pod"

    RELEASE_NAME=$RELEASE_NAME ./helper/restore_clamav_db_backup_if_possible_and_needed.sh

    if $use_mas; then
        ./helper/fix_mas_unable_to_validate_keycloak_cert.sh
        ./helper/patch_mas_disable_password_auth.sh
    fi

    printf 'Now visit https://%s\n' $(oc get route -o name | grep webclient- | xargs oc get -o jsonpath='{.status.ingress[0].host}')
}


main
