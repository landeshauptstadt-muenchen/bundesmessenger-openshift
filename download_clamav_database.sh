#!/bin/bash

cd "$(dirname "0")"

main(){
    local schadcodescanner_pod=$(
        oc get po -l "app.kubernetes.io/component==schadcodescanner" -o name | head -1
    )
    if [ -z "$schadcodescanner_pod" ]; then
        echo "No schadcodescanner pod detected in the current namespace"
        exit 1
    else
        echo "Wait for the schadcodescanner to become ready ..."
        oc wait --for=condition=Ready --timeout=1h "$schadcodescanner_pod"
        local command='cd /clamav/database; find . -type f | tar -cf - -T - | base64 -w0'
        2>/dev/null oc rsh "$schadcodescanner_pod" bash -c "$command" |
            base64 -d > clamav_database.tar
        echo "clamav_database.tar downloaded"
    fi
}

main
