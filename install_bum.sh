#!/bin/bash -e

BUM_NS=${BUM_NS:-bum}
RELEASE_NAME=${RELEASE_NAME:-bum}

die(){ echo "FATAL: $*"; exit 1; }
cd "$(dirname "$0")"


gen_random_posgres_pw(){
    local charset='[:graph:]'
    local secretlength=24
    cat /dev/urandom | tr -dc "$charset" | tr -d '"\' | fold -w ${1:-$secretlength} | head -1
}

main(){
    if oc get ns -o name | cut -d / -f 2| grep -q "^$BUM_NS$"; then
        die "Namespace '$BUM_NS' already present. Delete it or define another NS by exporting BUM_NS."
    fi

    [ -n "$https_proxy" ] || die "https_proxy is undefined!"

    oc new-project "$BUM_NS"
    ./helper/create-ca-bundle.sh

    local random_postgres_pw=$(gen_random_posgres_pw)
    POSTGRESQL_PASSWORD=$random_postgres_pw envsubst < postgres.yaml.template |
        oc apply -f -

    ./keycloak-testinstance/setup_keycloak.sh

    local domain=$(
        oc get ingresscontrollers -n openshift-ingress-operator default -o \
            jsonpath='{.status.domain}')


    if grep -q '^\s*oidc_providers:' values.yaml.template; then
        local use_mas=false
    else
        local use_mas=true
    fi

    local values_file="$BUM_NS-values.yaml"
    cat values.yaml.template |
        BUM_NS=$BUM_NS \
        RELEASE_NAME=$RELEASE_NAME \
        DOMAIN=$domain \
        POSTGRESQL_PASSWORD=$random_postgres_pw \
        USE_MAS=$use_mas \
        envsubst '$BUM_NS,$RELEASE_NAME,$DOMAIN,$POSTGRESQL_PASSWORD,$USE_MAS,$https_proxy' |
            grep -v TEMPLATEHINT > "$values_file"
    helm install "$RELEASE_NAME" ./upstream/ --namespace "$BUM_NS" -f openshift.yaml -f "$values_file"

    RELEASE_NAME=$RELEASE_NAME ./finalize.sh
}

main
