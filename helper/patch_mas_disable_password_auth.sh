#!/bin/bash

# This patch is required as long as the related setting is hardcoded:
# https://gitlab.opencode.de/bwi/bundesmessenger/backend/helm-chart/-/blob/v1.6.1/templates/matrix-authentications-service/matrix-authentication-service-configuration.yaml#L83

main(){
    local auth_cm=$(oc get cm -l component==matrix-authentication-service -o name)
    local new_cm=$(
        oc get "$auth_cm" -o yaml |
        # this substitution could easily break in future bum releases
        # this drawback is accepted anyway because of the goal to
        # avoid introducing dependancy towards other tools like yq
        # which could manipulate structured yaml content in a more precise manner
        sed 's/enabled: true/enabled: false/g'
    )
    oc delete "$auth_cm"
    echo "$new_cm" | oc apply -f -
    oc get po -l app.kubernetes.io/component==matrix-authentication-service -o name | xargs oc delete --force

    echo Waiting for matrix-authentication-service to become ready
    oc get po -l app.kubernetes.io/component==matrix-authentication-service -o name | xargs oc wait --for=condition=Ready --timeout=1h
}

main
