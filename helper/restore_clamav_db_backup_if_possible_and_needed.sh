#!/bin/bash

# The following helper pod config will be used to restore
# the clamav database backup into the pristine pvc
SCHADCODESCANNER_HELPER_POD_CONFIG=$(cat << 'EOF'
apiVersion: v1
kind: Pod
metadata:
  name: schadcodescanner-db-mount
spec:
  containers:
  - image: $image # needs to be defined
    imagePullPolicy: IfNotPresent
    name: clamav
    command:
    - "/bin/bash"
    - "-c"
    - "sleep 1h"
    volumeMounts:
    - mountPath: /clamav/database
      name: signatures
  volumes:
  - name: signatures
    persistentVolumeClaim:
      claimName: $RELEASE_NAME-schadcodescanner
EOF
)


try_to_determine_release_name(){
    RELEASE_NAME=$(helm ls | grep bundesmessenger | cut -d $'\t' -f 1)
    if [ -z "$RELEASE_NAME" ] || [ $(wc -l <<< "$RELEASE_NAME") -gt 1 ]; then
        echo "Failed to determine RELEASE_NAME"
		exit 1
    fi
}


main(){
    [ -n "$RELEASE_NAME" ] || try_to_determine_release_name
    if [ -f clamav_database.tar ]; then
		# freshclam is in progress anyway
		echo "Give freshclam some time to complete and restore db from backup if that fails..."
		if oc wait --for=condition=Ready --timeout=1m "pod/$RELEASE_NAME-schadcodescanner-0"; then
            echo "... freshclam completed."
        else
            echo "... freshclam failed to complate; restoring from backup ..."
			oc scale statefulsets.apps "$RELEASE_NAME-schadcodescanner" --replicas 0
			local image=$(
				oc get statefulsets.apps $RELEASE_NAME-schadcodescanner -o \
					jsonpath='{.spec.template.spec.containers[0].image}'
				)
			local config=$( image=$image RELEASE_NAME=$RELEASE_NAME envsubst <<< "$SCHADCODESCANNER_HELPER_POD_CONFIG" )
			oc apply -f - <<< "$config"
			oc wait --for=condition=Ready --timeout=1h pod/schadcodescanner-db-mount
			cat clamav_database.tar |
				oc rsh schadcodescanner-db-mount bash -c 'tar -xf - -C /clamav/database/'
			oc delete --force pod/schadcodescanner-db-mount
			oc scale statefulsets.apps "$RELEASE_NAME-schadcodescanner" --replicas 1
		fi
    fi
}

main
