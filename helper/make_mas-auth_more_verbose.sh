#!/bin/bash

oc get statefulset.apps -l app.kubernetes.io/component==matrix-authentication-service -o name | xargs oc patch --type json --patch  '
[
  {
    "op": "add",
    "path": "/spec/template/spec/containers/0/env",
    "value": [
      {
        "name": "RUST_LOG",
        "value": "debug"
        # Valid levels from least to most verbose are `error`, `warn`, `info`, `debug` and `trace`
        # source: https://github.com/matrix-org/matrix-authentication-service/blob/main/docs/usage/cli/README.md#logging
      }
    ]
  }
]
'
