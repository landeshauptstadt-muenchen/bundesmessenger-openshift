#!/bin/bash

# This script solves the issue that the mas server is unable to communicate with the keycloak test instance due to certificate validation issues.

main(){
    oc create cm ca-cert
    oc patch cm ca-cert --type=json --patch='
        [
          {
            "op": "add",
            "path": "/metadata/labels",
            "value": {
              "config.openshift.io/inject-trusted-cabundle": "true"
            }
          }
        ]
    '

    local auth_statefulset=$(
        oc get statefulsets.apps -l app.kubernetes.io/component==matrix-authentication-service -o name
    )
    if oc get "$auth_statefulset" -o yaml | grep -q ca-cert-volume; then
        echo "'$auth_statefulset' have already been patched"
    else
        oc patch "$auth_statefulset" --type=json --patch '
            [
              {
                "op": "add",
                "path": "/spec/template/spec/volumes/-",
                "value": {
                  "name": "ca-cert-volume",
                  "configMap": {
                    "name": "ca-cert"
                  }
                }
              },
              {
                "op": "add",
                "path": "/spec/template/spec/containers/0/volumeMounts/-",
                "value": {
                  "mountPath": "/etc/ssl/certs/ca-certificates.crt",
                  "name": "ca-cert-volume",
                  "subPath": "ca-bundle.crt"
                }
              }
            ]
            '

        oc get po -l app.kubernetes.io/component==matrix-authentication-service -o name | xargs oc delete --force

        echo Waiting for matrix-authentication-service to become ready
        oc get po -l app.kubernetes.io/component==matrix-authentication-service -o name | xargs oc wait --for=condition=Ready --timeout=1h
    fi
}

main
