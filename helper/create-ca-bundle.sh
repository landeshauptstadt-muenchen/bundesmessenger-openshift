#!/bin/bash

main(){
    oc create cm ca-bundle
    oc label cm ca-bundle config.openshift.io/inject-trusted-cabundle=true
}

main
