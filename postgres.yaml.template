apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: postgresql
    app.kubernetes.io/component: postgresql
    app.kubernetes.io/instance: postgresql
  name: postgresql
spec:
  replicas: 1
  selector:
    matchLabels:
      deployment: postgresql
  template:
    metadata:
      labels:
        deployment: postgresql
    spec:
      containers:
      - name: postgresql
        env:
        - name: POSTGRESQL_DATABASE
          value: example
        - name: POSTGRESQL_PASSWORD
          value: "$POSTGRESQL_PASSWORD"
        - name: POSTGRESQL_USER
          value: example
        - name: LANG
          value: C
        image: docker.io/bitnami/postgresql
        volumeMounts:
        - mountPath: /docker-entrypoint-initdb.d
          name: postgresql-volume-1
        - mountPath: /docker-entrypoint-preinitdb.d
          name: postgresql-volume-2
        - mountPath: /bitnami/postgresql
          name: postgresql-volume-3
      volumes:
      - emptyDir: {}
        name: postgresql-volume-1
      - emptyDir: {}
        name: postgresql-volume-2
      - emptyDir: {}
        name: postgresql-volume-3
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: postgresql
    app.kubernetes.io/component: postgresql
    app.kubernetes.io/instance: postgresql
  name: simple-postgresql
spec:
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - name: 5432-tcp
    port: 5432
    protocol: TCP
    targetPort: 5432
  selector:
    deployment: postgresql
  sessionAffinity: None
  type: ClusterIP
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: postgresql2
    app.kubernetes.io/component: postgresql2
    app.kubernetes.io/instance: postgresql2
  name: postgresql2
spec:
  replicas: 1
  selector:
    matchLabels:
      deployment: postgresql2
  template:
    metadata:
      labels:
        deployment: postgresql2
    spec:
      containers:
      - name: postgresql2
        env:
        - name: POSTGRESQL_DATABASE
          value: example
        - name: POSTGRESQL_PASSWORD
          value: "$POSTGRESQL_PASSWORD"
        - name: POSTGRESQL_USER
          value: example
        - name: LANG
          value: C
        image: docker.io/bitnami/postgresql
        volumeMounts:
        - mountPath: /docker-entrypoint-initdb.d
          name: postgresql2-volume-1
        - mountPath: /docker-entrypoint-preinitdb.d
          name: postgresql2-volume-2
        - mountPath: /bitnami/postgresql2
          name: postgresql2-volume-3
      volumes:
      - emptyDir: {}
        name: postgresql2-volume-1
      - emptyDir: {}
        name: postgresql2-volume-2
      - emptyDir: {}
        name: postgresql2-volume-3
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: postgresql2
    app.kubernetes.io/component: postgresql2
    app.kubernetes.io/instance: postgresql2
  name: simple-postgresql2
spec:
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - name: 5432-tcp
    port: 5432
    protocol: TCP
    targetPort: 5432
  selector:
    deployment: postgresql2
  sessionAffinity: None
  type: ClusterIP
