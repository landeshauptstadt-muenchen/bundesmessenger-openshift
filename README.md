# Über dieses Repository

Dieses Repository beinhaltet ein Testsetup zur Installation des [Bundesmessangers](https://gitlab.opencode.de/bwi/bundesmessenger/backend/helm-chart) auf einem Openshift Cluster mit folgenden Anforderungen:

* Installation in ein dediziertes Zielnamespace
* Nutzung der standard SecurityContextConstraints
* [Using arbitrary UIDs and GIDs](https://www.redhat.com/en/blog/a-guide-to-openshift-and-uids)

Konkret beinhaltet es kleinere Patches, die derzeit erforderlich sind, um den obigen Anforderungen gerecht zu werden.


# Voraussetzungen

Vorausgesetzt sind:

* Installiertes `oc` Tool
* Anmeldung am Cluster als Clusteradmin mittels `oc`
* Proxy ist bekannt und erforderlich (falls nicht erforderlich, sind manuell kleinere Änderungen vorzunehmen.)
* Das für die Kommunikation mit der [Keycloak-Testinstanz](./keycloak-testinstance/) erforderliche Zertifikat ist im Configmap `user-ca-bundle` im `ca-bundle.crt` (Namespace `openshift-config`) abzulegen oder [anderweitig dafür zu sorgen](./helper/create-ca-bundle.sh), dass es neben den üblichen Root-Zeritifikaten im ConfigMap `ca-bundle` landet.


# Nutzung

Für eine Standardinstallation genügt:

```bash
export https_proxy=... # Proxy definieren
git submodule update --init
./install_bum.sh
./download_clamav_database.sh # optional; siehe unten
```

Das Verhalten des Installers kann durch folgende Umgebungsvariablen beeinflusst werden:

* `BUM_NS`: Ziel Namespace (Default: `bum`)
* `RELEASE_NAME`: Release name (Default: `bum`)

Die Zugangsdaten für die Keycloak Testinstanz lauten:

* admin-interface:
  * username: admin
  * password: admin
* testuser:
  * user1:
      * name: user1
      * password: pw1
  * user2:
      * name: user2
      * password: pw2

Das oben erwähnte optionale Skript lädt die clamav Datenbank des Schadcodescanners in das Root-Verzeichnis dieses Repositories herunter.
Dieser Cache wird bei den nachfolgenden Installationen wiederverwendet.
Ohne diesem Cache-Mechanismus gerät man schnell in ein 24h Ratelimit, falls viele Test(re)installationen vorgenommen werden.

# Synapse admin

Die Synapse admin URL lässt sich mit folgendem Kommando ermitteln:

```bash
oc get $(oc get ingress -o name | grep synapse-admin) -o yaml | sed -n 's;.*host: ;https://;p'
```

Auf der Anmeldemaske ist dieselbe URL (enspricht adminAPIServerName) als Heimserver URL zu spezifizieren.

Eine direkte Anmeldung als synapse admin [via MAS ist derzeit nicht möglich](https://github.com/Awesome-Technologies/synapse-admin/issues/429).
Es [existieren aber Workarounds](https://github.com/Awesome-Technologies/synapse-admin/issues/429#issuecomment-2077184638).

# Fehlerbehebungen

## keycloak: invalid peer certificate

```bash
oc get statefulset.apps -l app.kubernetes.io/component==matrix-authentication-service -o name | xargs oc logs | grep 'invalid peer certificate'
[...] ERROR cli.run.init:metadata_cache.warm_up_and_run:metadata_cache.fetch{issuer=https://keycloak-[...]: mas_handlers::upstream_oauth2::cache: error=error trying to connect: invalid peer certificate: UnknownIssuer
```

Wenn der obige Fehler zu beobachten ist, sind die zur Validierung der Zertifikatskette exponierter Routen erforderlichen CAs zu platzieren in:
* ConfigMap: `cm/user-ca-bundle`
* key: `ca-bundle.crt`
* namespace: `openshift-config`

... oder der Fix [helper/fix_mas_unable_to_validate_keycloak_cert.sh](helper/fix_mas_unable_to_validate_keycloak_cert.sh) anzupassen.
