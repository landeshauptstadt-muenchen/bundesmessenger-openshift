#!/bin/bash

echo '##############'
echo '### IMPORT ###'
echo '##############'
/opt/keycloak/bin/kc.sh import --file /mnt/kc_stuff/kc_export.json

echo '##############'
echo '### START ####'
echo '##############'
/opt/keycloak/bin/kc.sh start-dev --log-level=INFO
# definable loglevels:
# https://www.keycloak.org/server/logging
