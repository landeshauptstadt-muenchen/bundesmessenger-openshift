## Folder content

* [kc-entrypoint.sh](kc-entrypoint.sh): Entrypoint script which will be used in the keycloak pod
* [kc_export.json.gz](kc_export.json.gz): Configured keycloak state (see [Generate keycloak export file](#generate-keycloak-scport-file))
* [setup_keycloak.sh](setup_keycloak.sh): Main script to launch the keycloak instance
* [show_urls.sh](show_urls.sh): Show keycloak urls
* [upstream_template.yaml](upstream_template.yaml): Template downloaded from [keycloak/keycloak-quickstarts](https://raw.githubusercontent.com/keycloak/keycloak-quickstarts/latest/openshift/keycloak.yaml). If deleted [setup_keycloak.sh](setup_keycloak.sh) will try to download the newest version.

## Generate keycloak export file

To create your own [kc_export.json.gz](kc_export.json.gz) follow these steps:

* launch a blank keycloak instance by either
  * following the [official keycloak getting-started guide]
  * commenting out the import line in [kc-entrypoint.sh](kc-entrypoint.sh) and rerunning [setup_keycloak.sh](setup_keycloak.sh)
* configure keycloak
  * login in admin-console with `admin` / `admin` (or your credentials if specified otherwise)
    * create realm `myrealm` (or another one)
    * create user `user1`
        * define password for `user1`: `pw1` in the `Credentials` tab
    * create user `user2`
        * define password for `user2`: `pw2` in the `Credentials` tab
    * create client following [matrix-keycloak-client-example]
      * client ID: matrix-authentication-service
      * next
      * checked `Client authentication`
      * next
        * `Valid redirect URIs`: `https://*` (broad whitelisting to test MAS and synapse oidc-origins)
        * `Web origins`: `/*`
      * save
      * the credentials tab reveals the created secret
        * this needs to be defined in `{{values.mas.upstream_oauth2_provider.client_secret}}`
* export the cofiguration:
  ```bash
  oc rsh dc/keycloak /opt/keycloak/bin/kc.sh export --file /tmp/kc_export.json
  oc rsh dc/keycloak cat /tmp/kc_export.json | gzip > kc_export.json.gz
  ```

## Notes

* inconsistancy of [matrix-keycloak-client-example]
  * `Access Type`: `confidential`
    * where can this setting be define in the [most recent keycloak version 24.0.1](https://www.keycloak.org/docs/latest/release_notes/)?
    * is this the equivalent to the `Client authentication` flag?

[matrix-keycloak-client-example]: https://matrix-org.github.io/matrix-authentication-service/setup/sso.html#keycloak
[official keycloak getting-started guide]: https://www.keycloak.org/getting-started/getting-started-openshift
