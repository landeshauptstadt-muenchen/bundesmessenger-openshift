#!/bin/bash

cd "$(dirname "$0")"

main(){
	[ -f upstream_template.yaml ] || curl https://raw.githubusercontent.com/keycloak/keycloak-quickstarts/latest/openshift/keycloak.yaml > upstream_template.yaml

	oc delete dc/keycloak # to make script idempotent

	oc process -f upstream_template.yaml \
		-p KEYCLOAK_ADMIN=admin \
		-p KEYCLOAK_ADMIN_PASSWORD=admin \
        -p NAMESPACE="_" \
        | oc create -f -
    zcat kc_export.json.gz > kc_export.json
    oc create cm kc-stuff --from-file=kc_export.json --from-file=kc-entrypoint.sh --dry-run=client -o yaml | oc apply -f -
    rm kc_export.json
	oc patch dc/keycloak --type=json --patch '
        [
          {
            "op": "add",
            "path": "/spec/template/spec/volumes/-",
            "value": {
              "name": "kc-stuff-volume",
              "configMap": {
                "name": "kc-stuff"
              }
            }
          },
          {
            "op": "add",
            "path": "/spec/template/spec/containers/0/volumeMounts/-",
            "value": {
              "mountPath": "/mnt/kc_stuff",
              "name": "kc-stuff-volume"
            }
          },
          {
            "op": "add",
            "path": "/spec/template/spec/containers/0/command",
            "value": [
              "sh",
              "/mnt/kc_stuff/kc-entrypoint.sh"
            ]
          },
          {
            "op": "remove",
            "path": "/spec/template/spec/containers/0/args"
          }
        ]
        '

    ./show_urls.sh
    echo Waiting for dc/keycloak to become available
    oc wait --for=condition=Available --timeout=1h dc/keycloak
}

main
